import { put_task_service,del_task_service,list_completed,list_todo } from "./main.js";
import { item_todo, item_completed } from "../modal/modal.js";
export let get_task=()=>{
    let name=document.getElementById("newTask").value;
    let time= Date.now();
    let check=false;
    return {
        name:name,
        time:time,
        check:check,
    }
}
export let render=(list,item_render,id)=>{
    let content_html=``;
    if(Array.isArray(list)){
        list.forEach((item)=>{content_html+=item_render(item.name)});
        document.getElementById(id).innerHTML=content_html;
        button_del_check(list,item_render,id);
    }else{}
 }
 //
 export let button_del_check=(list,item_render,id)=>{
    document.querySelectorAll(`#${id} li`).forEach((li,index_li)=>{
        li.querySelectorAll("button").forEach((btn,index_btn)=>{
             if(index_btn==0){// nut delete
                btn.onclick=()=>{
                    del_task_service(list[index_li].id);
                    list.splice(index_li,1);
                    render(list,item_render,id);
                }
             }else{ //nut check hoan thanh
                btn.onclick=()=>{
                    put_task_service({...list[index_li],check:(id=="todo"? true:false),time:new Date()});
                    ((id=="todo"?list_completed:list_todo)).push({...list[index_li],check:(id=="todo"? true:false),time: Date.now()});
                    list.splice(index_li,1);
                    render(list,item_render,id);
                    render((id=="todo"?list_completed:list_todo),(id=="todo"?item_completed:item_todo),(id=="todo"?"completed":"todo"));
                }
             }
        })   
    })
 }
 // 
 
