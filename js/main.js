
export let list_todo=[];
export let list_completed=[];
import {get_task,render,button_del_check} from "./controller.js";
import {item_todo,item_completed } from "../modal/modal.js";
var baseURL="https://62f8b78ae0564480352bf7a7.mockapi.io";

export let render_task_service=()=>{
    axios({
        url: `${baseURL}/todo`,
        method:"GET",
      })
      .then(function(res){
         list_completed=[];
         list_todo=res.data;
         for(var i=0;i<list_todo.length;i++){
            if(list_todo[i].check==true){
               list_completed.push(list_todo[i]);
               list_todo.splice(i,1);
               i--;
            }
         }  
         render(list_todo,item_todo,"todo");
         render(list_completed,item_completed,"completed");
      })
     
 }
 render_task_service();
 let push_task_service=(task)=>{
   axios({
      url: `${baseURL}/todo`,
      method:"POST",
      data:task,
    })
    .then(function(res){
        render_task_service();     
    })
 }
 export let del_task_service=(id)=>{
   axios({
      url: `${baseURL}/todo/${id}`,
      method:"DELETE",
    })
 }
export let put_task_service=(task)=>{
   axios({
      url: `${baseURL}/todo/${task.id}`,
      method:"PUT",
      data:task,
    })
 }
 document.getElementById("addItem").addEventListener("click",()=>{//add task
   let task=get_task();
   if(task.name!=""){
      push_task_service(task);
   }
 })

 document.querySelectorAll(".filter-btn a").forEach((a,index_a)=>{//filter
   if(index_a==0){a.addEventListener("click",()=>{
        render(list_todo,item_todo,"todo");  
        render(list_completed,item_completed,"completed");
    
   })}
   else if(index_a==1){a.addEventListener("click",()=>{
      let todo_name= list_todo.sort((a, b) => (a.name).localeCompare(b.name));
      render(todo_name,item_todo,"todo");
      let completed_name= list_completed.sort((a, b) => (a.name).localeCompare(b.name));
      render(completed_name,item_completed,"completed");
   })
   }
   else if(index_a==2){
      a.addEventListener("click",()=>{
         let todo_name= list_todo.sort((a, b) => (b.name).localeCompare(a.name));
         render(todo_name,item_todo,"todo");
         let completed_name= list_completed.sort((a, b) => (b.name).localeCompare(a.name));
         render(completed_name,item_completed,"completed");
      })
   }
   else if(index_a==3){
      a.addEventListener("click",()=>{
         let todo_time= list_todo.sort((a, b) => (a.time)-(b.time));
         render(todo_time,item_todo,"todo");
         let completed_time= list_completed.sort((a, b) =>(a.time)-(b.time));
         render(completed_time,item_completed,"completed");
      })
   }
 })

 

 