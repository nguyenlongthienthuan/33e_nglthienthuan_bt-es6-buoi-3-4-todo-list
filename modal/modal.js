export let item_todo=(name)=>{
    return `
    <li>${name} 
    <div class="buttons">
    <button class="remove"><i class="fa fa-trash-alt"></i></button> 
    <button class="complete"><i class="fa fa-check-circle far"></i></button>
    </div>
    </li>
    `
}
export let item_completed=(name)=>{
    return `
    <li><span>${name}</span> 
    <div class="buttons">
    <button class="remove"><i class="fa fa-trash-alt"></i></button> 
    <button class="complete"><i class="fa fa-check-circle fas"></i></button>
    </div>
    </li>
    `
}